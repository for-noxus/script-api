package nox.api.graphscript;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.BooleanSupplier;

public abstract class Node<k extends Node> {

    private List<k> childNodes;
    private String message;

    private boolean aborted = false;
    private boolean completed = false;

    private String abortedReason = "Node has been aborted.";
    private String completedMessage;
    private BooleanSupplier onlyExecutionCondition;
    private BooleanSupplier andExecutionCondition;

    public Node() {

    }

    public Node(List<k> children, String message) {
        this.childNodes = children;
        this.message = message;
    }

    public Node(k child, String message) { this(Arrays.asList(child), message); }

    public void setChildNodes(List<k> nodes) {
        this.childNodes = nodes;
    }

    public void setChildNode(k node) {
        this.setChildNodes(Arrays.asList(node));
    }

    public List<k> getChildNodes() {
        return this.childNodes;
    }

    public k getNext() {
        if (childNodes == null || childNodes.size() == 0) return null;
        return childNodes.stream().filter(Objects::nonNull).filter(Node::isValid).findFirst().orElse(null);
    }

    public String getMessage() {
        return message;
    }

    public boolean isValid() {
        if (onlyExecutionCondition != null)
            return onlyExecutionCondition.getAsBoolean();

        return baseExecutionCondition() && (andExecutionCondition == null || andExecutionCondition.getAsBoolean());
    }

    protected abstract boolean baseExecutionCondition();
    public abstract int execute() throws InterruptedException;

    public boolean isAborted() { return aborted; }

    public String getAbortedReason() { return abortedReason; }

    protected void abort(String reason) {
        this.abortedReason = reason;
        this.aborted = true;
    }

    protected void complete(String completedMessage) {
        this.completedMessage = completedMessage;
        this.completed = true;
    }

    public void reactivate() {
        this.aborted = false;
        this.completed = false;
        this.abortedReason = "";
        this.completedMessage = "";
    }

    public String getCompletedMessage() {
        return this.completedMessage;
    }

    public boolean isCompleted() {
        return this.completed;
    }

    public Node hasChild(k child) { this.childNodes = Arrays.asList(child); return this; }
    public Node hasChildren(List<k> children) { this.childNodes = children; return this; }
    public Node hasMessage(String message) { this.message = message; return this; }
    public Node onlyExecuteIf(BooleanSupplier condition) { this.onlyExecutionCondition = condition; return this; }
    public Node andExecuteIf(BooleanSupplier condition) { this.andExecutionCondition = condition; return this; }
}
